import os
import sys
import psycopg2
from ext.config import Config
from ext.logger import logger
from datetime import datetime as dt

config = Config()


class Database:
    def __init__(self):
        self.conn = None
        self.host = config.DB_HOST
        self.database = config.DB_NAME
        self.username = config.DB_USER
        self.password = config.DB_PASS

    def open_connection(self):
        # open connection in every operation?
        try:
            self.conn = psycopg2.connect(
                host=self.host,
                user=self.username,
                password=self.password,
                dbname=self.database,
            )
            logger.success("Connection opened successfully.")
        except psycopg2.DatabaseError as e:
            logger.critical(f"Something is wrong to connect with database {e}")
            sys.exit()

    def insert_user_info(self, info):
        self.open_connection()
        with self.conn.cursor() as cur:
            cur.execute("select * from users where chat_id=%s", (str(info["chat_id"]),))

            check = cur.fetchone()

            if check is None:
                cur.execute(
                    """insert into users (chat_id, username, firstname) values (%s, %s, %s)""",
                    (info["chat_id"], info["username"], info["firstname"]),
                )

            else:
                user = (
                    info["username"]
                    if info["username"] is not None
                    else info["firstname"]
                )
                logger.info(f"User {user} exists.")

            cur.execute(
                """insert into commands (chat_id, command) values(%s, %s)""",
                (info["chat_id"], info["message"]),
            )

            self.conn.commit()

    def insert_coins(self, data):
        self.open_connection()
        with self.conn.cursor() as cur:
            cur.execute(
                """insert into coins (dolar, euro, libra, peso_argentino, ouro, bitcoin, ethereum, dogecoin, dash, ripple, litecoin, dolar_tur)
                values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                (
                    data[0],
                    data[1],
                    data[2],
                    data[3],
                    data[4],
                    data[5],
                    data[6],
                    data[7],
                    data[8],
                    data[9],
                    data[10],
                    data[11],
                ),
            )

            self.conn.commit()

            logger.success(f"Data inserted: {data} at: {dt.now()}")

    def get_coins(self, coin):
        data = {}
        self.open_connection()
        with self.conn.cursor() as cur:
            cur.execute(
                f"SELECT {coin}, max(datetime) datetime FROM coins GROUP BY {coin} ORDER BY max(datetime) DESC LIMIT 2;"
            )

            coins_data = cur.fetchall()

            data["latest"] = {"value": coins_data[0][0], "datetime": coins_data[0][1]}

            if len(coins_data) > 1:
                data["next_from_latest"] = {
                    "value": coins_data[1][0],
                    "datetime": coins_data[1][1],
                }

            if len(data) > 0:
                logger.success(f"Data returned: coin: {coin}, data {data}")
                return data
            else:
                logger.critical(f"Fail to return data for: {coin}")
