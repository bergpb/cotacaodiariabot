import argparse
import sched
import time
from datetime import datetime, timedelta

import cfscrape
import psycopg2
from bs4 import BeautifulSoup

from cdbconnection import Database
from ext.config import Config
from ext.logger import logger

scraper = cfscrape.create_scraper()
sched = sched.scheduler(timefunc=time.time)

cn = Database()
config = Config()


parser = argparse.ArgumentParser()
parser.add_argument(
    '-t', '--type', metavar='type', required=True, help='Define how to run the service: sched or cronjob'
)
args = parser.parse_args()


def enqueue(action):
    new_delay = datetime.now().replace(second=0, microsecond=0)
    new_delay += timedelta(minutes=int(config.DELAY))
    logger.info(f"Next call will be made at {new_delay}")

    sched.enterabs(new_delay.timestamp(), priority=config.PRIORITY, action=action)


def scrapy():
    cot_coins = []

    logger.info("Scrapy started...")

    coins_url = [
        "https://dolarhoje.com",
        "https://dolarhoje.com/euro-hoje/",
        "https://dolarhoje.com/libra-hoje/",
        "https://dolarhoje.com/peso-argentino/",
        "https://dolarhoje.com/ouro-hoje/",
        "https://dolarhoje.com/bitcoin-hoje/",
        "https://dolarhoje.com/ethereum/",
        "https://dolarhoje.com/dogecoin-hoje/",
        "https://dolarhoje.com/dash/",
        "https://dolarhoje.com/ripple-hoje/",
        "https://dolarhoje.com/litecoin/",
        "https://dolarhoje.com/dolar-turismo/",
    ]

    for url in coins_url:
        html_doc = scraper.get(url).content
        soup = BeautifulSoup(html_doc, "html.parser")
        cot_coins.append(float(soup.find(id="nacional").get("value").replace(",", ".")))

    logger.success(f"Scrapy finished! {cot_coins}")

    try:
        cn.insert_coins(cot_coins)
        logger.success(f"Scrapy saved! {cot_coins}")
    except Exception as e:
        logger.warning(f"Fail to save data with scrapy: {e}")

    if args.type == 'sched':
        enqueue(scrapy)


if __name__ == "__main__":
    if args.type == 'sched':
        logger.info(
            f"The DELAY was set to {config.DELAY} minutes and PRIORITY to {config.PRIORITY}"
        )
        enqueue(scrapy)
        sched.run(blocking=True)
    elif args.type == 'cronjob':
        scrapy()
    else:
        raise ValueError('Type only can be one of these options: sched or cronjob')
