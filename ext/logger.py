import os
from loguru import logger

from ext.config import Config

config = Config()
formatter = (
    "[{level.icon}{level:^10}] {time:YYYY-MM-DD hh:mm:ss} {file} - {name}: {message}"
)

log_dir = os.path.abspath('../logger.log')

try:
    level = config.LOGGER_LEVEL
except AttributeError:
    level = "INFO"
try:
    file_name = config.LOGGER_FILE
except AttributeError:
    file_name = log_dir

logger.add(file_name, format=formatter, level=level, rotation="500 MB", colorize=True)

if __name__ == "__main__":
    logger.debug("debug message")
    logger.info("info message")
    logger.warning("warn message")
    logger.error("error message")
    logger.critical("critical message")
