import os
from dotenv import load_dotenv

load_dotenv()


class Config:
    ENV = os.getenv("ENV", "development")
    DB_HOST = os.getenv("DB_HOST")
    DB_NAME = os.getenv("DB_NAME")
    DB_USER = os.getenv("DB_USER")
    DB_PASS = os.getenv("DB_PASS")

    LOGGER_LEVEL = os.getenv("LOGGER_LEVEL")
    LOGGER_FILE = os.getenv("LOGGER_FILE")

    DELAY = os.getenv("DELAY")
    PRIORITY = os.getenv("PRIORITY")
