# Cotação Diária Telegram Bot

Telegram bot to scrapy data from [dolarhoje.com](https://dolarhoje.com) and interact with users.

### Requirements:

  - `make`, `docker` and `docker compose (v2)`

### Instructions Development (with virtualenv and docker compose):

  - Clone project,
  - Enter in project folder,
  - Create a virtualenv: `python3 -m venv .venv`
  - Activate env: `source .venv/bin/activate`
  - Install packages: `pip install -r requirements-dev.txt`
  - Create a `.env` base in `.env.example` with command `cp -n .env.example .env` changing with your variables,
  - Run command `make build` to build services,
  - Run command `make up` to start services,
  - Check logs with `make logs`,

Obs: Database migrations will run automatically when cdbbot service starts.


### Instructions Production (with Docker and docker-compose):

  - Clone project,
  - Enter in project folder,
  - Create a `.env` base in `.env.example` with command `cp -n .env.example .env` changing with your variables,
  - Run command `make build` to build services,
  - Run command `make up` to start services,
  - Create database tables with command: `make migrate`,
  - Check logs with `make logs`,


Running tests:

  - python -m unittest discover --verbose

To do:

  - [x] Docker;
  - [x] docker-compose;
  - [x] Create tests to scripts;
  - [x] Create tests to bot;
  - [x] Change to python-telegram-bot lib;
  - [x] Implement logging;
  - [x] Create a job to scrapy coins;
  - [x] Live reload in container using nodemon in docker container


### Useful commands:

Creating database backup:

  - docker exec -t container_name_or_id pg_dumpall -c -U psql_user > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql


Restoring database backup:

  - cat dump_*.sql | docker exec -i container_name_or_id psql -U psql_user psql_database

