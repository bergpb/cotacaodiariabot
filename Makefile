TAG  := ${TAG}
NAME := registry.gitlab.com/bergpb/cotacaodiariabot

compose_file := docker-compose.yml

ifeq ($(ENV),development)
	compose_file := docker-compose-dev.yml
endif

all: build up logs

install:
	@poetry install

test:
	@poetry shell && pytest -vv

format:
	@black cdbbot cdbconnection cdbservice cdbnotifications tests bot.py config.py create_tables.py logger.py

check:
	@black cdbbot cdbconnection cdbservice cdbnotifications tests bot.py config.py create_tables.py logger.py --check

clean:
	@py3clean .

migrate:
	@docker compose -f $(compose_file) exec cdbbot python create_tables.py;

scrapy:
	@docker compose -f $(compose_file) exec cdbbot python -c "from cdbservice import __main__; __main__.scrapy()";

build:
	@docker compose -f $(compose_file) build;

up:
	@docker compose -f $(compose_file) up -d;

logs:
	@docker compose -f $(compose_file) logs -f;

restart: | down up

down:
	@docker compose -f $(compose_file) down;

connect-db:
	@docker compose -f $(compose_file) exec cdbdatabase psql cotacaodiaria cotacaodiaria

multiarch:
	docker buildx build --push --platform linux/amd64,linux/arm64 --tag ${NAME}:${TAG} -f ./docker/Dockerfile .;
	docker buildx build --push --platform linux/amd64,linux/arm64 --tag ${NAME}:latest -f ./docker/Dockerfile .;

# db migrate
# kubectl exec -it pods/cotacaodiaria-bot-858f7d7c55-p8dr5 -- /bin/sh -c "python create_tables.py"

# db backup (data inserted only)
# kubectl exec -it pods/cotacaodiaria-bot -- /bin/sh/ -c "pg_dump --column-inserts --data-only" > dump_$(date).sql

# db restore
# cat dump_$(date).sql | kubectl exec -i pods/cotacaodiaria-db-68cd7c6ff4-j484f -- psql -U cotacaodiariabot -d cotacaodiariabot