all_coins = [
    "dolar",
    "dolar_tur",
    "euro",
    "libra",
    "peso_argentino",
    "ouro",
    "bitcoin",
    "ethereum",
    "dogecoin",
    "dash",
    "ripple",
    "litecoin",
]

coins_data = {
    "dolar": {"name": "Dólar", "image": "https://dolarhoje.com/img/icon-dolar.gif"},
    "dolar_tur": {
        "name": "Dólar Turismo",
        "image": "https://dolarhoje.com/img/icon-dolar.gif",
    },
    "euro": {"name": "Euro", "image": "https://dolarhoje.com/img/icon-euro.gif"},
    "libra": {"name": "Libra", "image": "https://dolarhoje.com/img/icon-libra.gif"},
    "peso_argentino": {
        "name": "Peso Argentino",
        "image": "https://dolarhoje.com/img/icon-peso-argentino.gif",
    },
    "ouro": {"name": "Ouro", "image": "https://dolarhoje.com/img/icon-ouro.gif"},
    "bitcoin": {
        "name": "Bitcoin",
        "image": "https://dolarhoje.com/img/icon-bitcoin.gif",
    },
    "ethereum": {
        "name": "Ethereum",
        "image": "https://dolarhoje.com/img/icon-ethereum.png",
    },
    "dogecoin": {
        "name": "Dogecoin",
        "image": "https://dolarhoje.com/img/icon-dogecoin.png",
    },
    "dash": {"name": "Dash", "image": "https://dolarhoje.com/img/icon-dash.png"},
    "ripple": {"name": "Ripple", "image": "https://dolarhoje.com/img/icon-ripple.png"},
    "litecoin": {
        "name": "Litecoin",
        "image": "https://dolarhoje.com/img/icon-litecoin.gif",
    },
}
