import os
import re
import logging
import locale
from uuid import uuid4
from .data import coins_data, all_coins
from functools import wraps
from unicodedata import normalize
from cdbconnection import Database
from telegram import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    ChatAction,
    InlineQueryResultArticle,
    ParseMode,
    InputTextMessageContent,
    Update,
)
from telegram.ext import (
    Updater,
    InlineQueryHandler,
    CommandHandler,
    MessageHandler,
    Filters,
    CallbackContext,
)


locale.setlocale(locale.LC_MONETARY, "pt_BR.UTF-8")

cn = Database()
BOT_TOKEN = os.getenv("BOT_TOKEN")

keyboard = [
    ["Dólar", "Dólar Turismo", "Euro"],
    ["Libra", "Peso Argentino", "Ouro"],
    ["Bitcoin", "Ethereum", "Dogecoin"],
    ["Dash", "Ripple", "Ajuda"],
]

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)


def convert_currency(coin, grouping=True):
    """Convert currency into PT-BR"""
    return locale.currency(coin["latest"]["value"], grouping=grouping)


def return_search(query, data):
    """Get values from search and return create a list from they"""
    values = []
    for i in data:
        if i.startswith(query):
            values.append(i)
    return values


def normalize_string(string: str):
    """Remove special characters from string"""
    return normalize("NFKD", string).encode("ASCII", "ignore").decode("ASCII").lower()


def return_status(data):
    """Check latest value and compare with next to latest value
    returning emoji to indicate up or down"""
    try:
        if data["latest"]["value"] > data["next_from_latest"]["value"]:
            return "⬆"
        elif data["latest"]["value"] < data["next_from_latest"]["value"]:
            return "⬇"
    except Exception as e:
        logger.warning(f"Error: {e}")
        return ""


def save_info(f):
    """Save info from user"""

    @wraps(f)
    def save_data(update, *args, **kwargs):
        logger.info(
            f"""Username: {update.message.chat.username}, Command: {update.message.text}, Datetime: {update.message.date}"""
        )

        user_info = {
            "chat_id": update.message.chat.id,
            "message": update.message.text,
            "username": update.message.chat.username,
            "firstname": update.message.chat.first_name,
        }

        cn.insert_user_info(user_info)
        return f(update, *args, **kwargs)

    return save_data


def check_and_send_message(coin, msg, update, context):
    if coin:
        update.message.reply_text(
            msg,
            reply_markup=ReplyKeyboardMarkup(keyboard),
            parse_mode=ParseMode.MARKDOWN,
        )
    else:
        logger.warning(f"Data from db is empty: {coin}")
        update.message.reply_text(
            f"""💰 Cotação Diária Bot 💰
*Ops!* Tivemos uma falha ao retornar esta informação.
Tente novamente mais tarde.""",
            reply_markup=ReplyKeyboardMarkup(keyboard),
            parse_mode=ParseMode.MARKDOWN,
        )


def send_typing_action(f):
    """Sends typing action while processing func command."""

    @wraps(f)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(
            chat_id=update.effective_message.chat_id, action=ChatAction.TYPING
        )
        return f(update, context, *args, **kwargs)

    return command_func


def inlinequery(update: Update, context: CallbackContext) -> None:
    results = []

    user_search = normalize_string(update.inline_query.query)

    search_query = return_search(user_search, all_coins)

    if len(search_query) > 0:
        for search in search_query:
            coin = cn.get_coins(search)
            status = return_status(coin)

            coin_name = coins_data[search]["name"]
            coin_img = coins_data[search]["image"]
            coin_last_update = coin["latest"]["datetime"].strftime("%d/%m/%Y %H:%M")

            if search == "dogecoin":
                coin_value = "{:.6f}".format(coin["latest"]["value"]).replace(".", ",")
            else:
                coin_value = convert_currency(coin)

            results.append(
                InlineQueryResultArticle(
                    id=uuid4(),
                    title=coin_name,
                    description="Cotação {}: {} {}".format(
                        coin_name, coin_value, status
                    ),
                    thumb_url=coin_img,
                    input_message_content=InputTextMessageContent(
                        message_text="""💰 *Cotação Diária Bot* 💰
Cotação [{}]({}): *{}* {}
Última atualização: {}""".format(
                            coin_name, coin_img, coin_value, status, coin_last_update
                        ),
                        parse_mode=ParseMode.MARKDOWN,
                    ),
                )
            )

    update.inline_query.answer(results)


@save_info
@send_typing_action
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""

    username = update.message.chat.username
    first_name = update.message.chat.first_name

    user = username if username is not None else first_name
    update.message.reply_text(
        f"""
💰 *Cotação Diária Bot* 💰
Bem vindo *{user}*.
Selecione os comandos entre o teclado abaixo:
""",
        reply_markup=ReplyKeyboardMarkup(keyboard),
        parse_mode=ParseMode.MARKDOWN,
    )


@save_info
@send_typing_action
def get_dolar(update: Update, context: CallbackContext) -> None:
    dolar = cn.get_coins("dolar")
    status = return_status(dolar)

    formated_dolar = convert_currency(dolar)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Dólar: *{}* {}
Última atualização: {}
""".format(
        formated_dolar,
        status,
        dolar["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(dolar, msg, update, context)


@save_info
@send_typing_action
def get_dolar_turismo(update: Update, context: CallbackContext) -> None:
    dolar_tur = cn.get_coins("dolar_tur")
    status = return_status(dolar_tur)

    formated_dolar_tur = convert_currency(dolar_tur)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Dólar Turismo: *{}* {}
Última atualização: {}
""".format(
        formated_dolar_tur,
        status,
        dolar_tur["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(dolar_tur, msg, update, context)


@save_info
@send_typing_action
def get_euro(update: Update, context: CallbackContext) -> None:

    euro = cn.get_coins("euro")
    status = return_status(euro)

    formated_euro = convert_currency(euro)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Euro: *{}* {}
Última atualização: {}
""".format(
        formated_euro,
        status,
        euro["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(euro, msg, update, context)


@save_info
@send_typing_action
def get_libra(update: Update, context: CallbackContext) -> None:

    libra = cn.get_coins("libra")
    status = return_status(libra)

    formated_libra = convert_currency(libra)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Libra: *{}* {}
Última atualização: {}
""".format(
        formated_libra,
        status,
        libra["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(libra, msg, update, context)


@save_info
@send_typing_action
def get_peso(update: Update, context: CallbackContext) -> None:

    peso = cn.get_coins("peso_argentino")
    status = return_status(peso)

    formated_peso = convert_currency(peso)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Peso Argentino: *{}* {}
Última atualização: {}
""".format(
        formated_peso,
        status,
        peso["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(peso, msg, update, context)


@save_info
@send_typing_action
def get_ouro(update: Update, context: CallbackContext) -> None:

    ouro = cn.get_coins("ouro")
    status = return_status(ouro)

    formated_ouro = convert_currency(ouro)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Ouro: *{}* {}
Última atualização: {}
""".format(
        formated_ouro,
        status,
        ouro["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(ouro, msg, update, context)


@save_info
@send_typing_action
def get_bitcoin(update: Update, context: CallbackContext) -> None:

    bitcoin = cn.get_coins("bitcoin")
    status = return_status(bitcoin)

    formated_bitcoin = convert_currency(bitcoin)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Bitcoin: *{}* {}
Última atualização: {}
""".format(
        formated_bitcoin,
        status,
        bitcoin["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(bitcoin, msg, update, context)


@save_info
@send_typing_action
def get_ethereum(update: Update, context: CallbackContext) -> None:

    ethereum = cn.get_coins("ethereum")
    status = return_status(ethereum)

    formated_ethereum = convert_currency(ethereum)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Ethereum: *{}* {}
Última atualização: {}
""".format(
        formated_ethereum,
        status,
        ethereum["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(ethereum, msg, update, context)


@save_info
@send_typing_action
def get_dogecoin(update: Update, context: CallbackContext) -> None:

    # only dogecoin use replace to better decimal places
    dogecoin = cn.get_coins("dogecoin")
    status = return_status(dogecoin)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Dogecoin: *R$ {:.6f}* {}
Última atualização: {}
""".format(
        dogecoin["latest"]["value"],
        status,
        dogecoin["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    ).replace(
        ".", ","
    )

    check_and_send_message(dogecoin, msg, update, context)


@save_info
@send_typing_action
def get_dash(update: Update, context: CallbackContext) -> None:

    dash = cn.get_coins("dash")
    status = return_status(dash)

    formated_dash = convert_currency(dash)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Dash: *{}* {}
Última atualização: {}
""".format(
        formated_dash,
        status,
        dash["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(dash, msg, update, context)


@save_info
@send_typing_action
def get_ripple(update: Update, context: CallbackContext) -> None:

    ripple = cn.get_coins("ripple")
    status = return_status(ripple)

    formated_ripple = convert_currency(ripple)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Ripple: *{}* {}
Última atualização: {}
""".format(
        formated_ripple,
        status,
        ripple["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(ripple, msg, update, context)


@save_info
@send_typing_action
def get_litecoin(update: Update, context: CallbackContext) -> None:

    litecoin = cn.get_coins("litecoin")
    status = return_status(litecoin)

    formated_litecoin = convert_currency(litecoin)

    msg = """
💰 *Cotação Diária Bot* 💰
Cotação Litecoin: R$ *{}* {}
Última atualização: {}
""".format(
        formated_litecoin,
        status,
        litecoin["latest"]["datetime"].strftime("%d/%m/%Y %H:%M"),
    )

    check_and_send_message(litecoin, msg, update, context)


@save_info
@send_typing_action
def help(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""

    update.message.reply_text(
        """💰 *Cotação Diária Bot* 💰
Use o teclado para ter acesso aos comandos.
Novidade: Use o modo inline para buscar moedas em chats ou grupos.
Ex: @cotacaodiariabot dolar""",
        reply_markup=ReplyKeyboardMarkup(keyboard),
        parse_mode=ParseMode.MARKDOWN,
    )


@save_info
@send_typing_action
def command_not_found(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""

    update.message.reply_text(
        """💰 *Cotação Diária Bot* 💰
Ops! Comando não encontrado.
Use o teclado para ter acesso aos comandos.
Experimente também o modo inline.
Ex: @cotacaodiariabot dolar""",
        reply_markup=ReplyKeyboardMarkup(keyboard),
        parse_mode=ParseMode.MARKDOWN,
    )


def main():
    """Start the bot."""
    updater = Updater(BOT_TOKEN, use_context=True)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(MessageHandler(Filters.text("Dólar"), get_dolar))
    dp.add_handler(MessageHandler(Filters.text("Dólar Turismo"), get_dolar_turismo))
    dp.add_handler(MessageHandler(Filters.text("Euro"), get_euro))
    dp.add_handler(MessageHandler(Filters.text("Libra"), get_libra))
    dp.add_handler(MessageHandler(Filters.text("Peso Argentino"), get_peso))
    dp.add_handler(MessageHandler(Filters.text("Ouro"), get_ouro))
    dp.add_handler(MessageHandler(Filters.text("Bitcoin"), get_bitcoin))
    dp.add_handler(MessageHandler(Filters.text("Ethereum"), get_ethereum))
    dp.add_handler(MessageHandler(Filters.text("Dogecoin"), get_dogecoin))
    dp.add_handler(MessageHandler(Filters.text("Dash"), get_dash))
    dp.add_handler(MessageHandler(Filters.text("Ripple"), get_ripple))
    dp.add_handler(MessageHandler(Filters.text("Litecoin"), get_litecoin))
    dp.add_handler(MessageHandler(Filters.text("Ajuda"), help))

    dp.add_handler(MessageHandler(Filters.text, command_not_found))
    dp.add_handler(MessageHandler(Filters.command, command_not_found))

    dp.add_handler(InlineQueryHandler(inlinequery))

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    from db import create_tables, run_migrations;
    create_tables();
    run_migrations();
    main()
