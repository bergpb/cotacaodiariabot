import psycopg2

from ext.config import Config
from ext.logger import logger

config = Config()


class Migrations():
    def __init__(self, host, database, user, password):
        self._db = psycopg2.connect(
            host=host, database=database, user=user, password=password
        )

    def create_table_users(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                CREATE TABLE IF NOT EXISTS users (
                    id serial primary key,
                    chat_id varchar(20) not null,
                    username varchar(20),
                    firstname varchar(20),
                    datetime timestamp default current_timestamp,
                    unique (chat_id)
                );
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(f"Oops! Exception ocurred: {type(error)}: {error}")

    def create_table_commands(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                CREATE TABLE IF NOT EXISTS commands (
                    id serial primary key,
                    chat_id varchar(20) not null,
                    command varchar(20),
                    datetime timestamp default current_timestamp,
                    foreign key (chat_id) references users(chat_id)
                );
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )

    def create_table_coins(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                CREATE TABLE IF NOT EXISTS coins (
                    id serial primary key,
                    dolar real,
                    euro real,
                    libra real,
                    peso_argentino real,
                    ouro real,
                    bitcoin real,
                    ethereum real,
                    dogecoin real,
                    dash real,
                    riple real,
                    litecoin real,
                    datetime timestamp default current_timestamp
                );
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )

    def alter_chatid_in_users(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                ALTER TABLE users ALTER COLUMN chat_id TYPE VARCHAR(100);
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )

    def alter_chatid_in_commands(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                ALTER TABLE commands ALTER COLUMN chat_id TYPE VARCHAR(100);
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )

    def alter_command_in_commands(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                ALTER TABLE commands ALTER COLUMN command TYPE VARCHAR(200);
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )

    def add_field_dolar_turismo_in_coins_table(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                ALTER TABLE coins ADD COLUMN IF NOT EXISTS dolar_tur real;
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )

    def rename_ripple_column_in_coins(self):
        try:
            cur = self._db.cursor()
            cur.execute(
                """
                ALTER TABLE coins RENAME COLUMN riple TO ripple;
                """
            )

            self._db.commit()
            cur.close()

        except Exception as error:
            logger.warning(
                f"Oops! An exception on type {type(error)} has occured: {error}"
            )


con = Migrations(
    host=config.DB_HOST,
    database=config.DB_NAME,
    user=config.DB_USER,
    password=config.DB_PASS,
)

def create_tables():
    con.create_table_coins()
    con.create_table_users()
    con.create_table_commands()

def run_migrations():
    con.alter_chatid_in_users()
    con.alter_chatid_in_commands()
    con.alter_command_in_commands()
    con.add_field_dolar_turismo_in_coins_table()
    con.rename_ripple_column_in_coins()
