import os
import config
from unittest import TestCase
from db.db_config import check_database
from scrapy import get_coins

db = config.settings()

update = {
    "message": {
        "date": 1570654571,
        "chat": {"id": 24774270, "username": "bergpb", "first_name": "__bergpb__",},
        "text": "/start",
        "from": {
            "id": 24774270,
            "first_name": "__bergpb__",
            "is_bot": False,
            "username": "bergpb",
            "language_code": "en",
        },
    },
}


class TestScripts(TestCase):
    def test_database_creation(self):
        import os

        if os.path.exists(db):
            self.assertEqual(check_database(), "Database already exists.")
        else:
            self.assertEqual(check_database(), "Database created.")

    def test_scrapy_coins(self):
        self.assertEqual(get_coins(), "Success to get coins.")
